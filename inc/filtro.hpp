#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <iostream>

using namespace std;

class Filtro {
	private:
		int div;
		int tamanho;

	public:
		Filtro();
		Filtro(int div, int tamanho);

		int getDiv();
		void setDiv(int div);

		int getTamanho();
		void setTamanho(int tamanho);
};

#endif