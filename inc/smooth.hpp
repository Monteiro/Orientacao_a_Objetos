#ifndef SMOOTH_HPP
#define SMOOTH_HPP

#include "filtro.hpp"

using namespace std;

class Smooth : public Filtro {
	public:
		Smooth();
		char * Filtra(char *pixels, int largura, int altura, int * max);
};
#endif