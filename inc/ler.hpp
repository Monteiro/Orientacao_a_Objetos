#ifndef LER_HPP
#define LER_HPP

#include "valores.hpp"

using namespace std;

class Ler : public Valores {

	private:
		ifstream * entrada;

	public:
		Ler();
		Ler(ifstream * entrada);

		ifstream * getEntrada();
		void setEntrada(ifstream * entrada);

		int Leitura(const char * nome);

};
#endif