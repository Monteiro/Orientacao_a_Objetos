#ifndef VALORES_HPP
#define VALORES_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

class Valores {
	private:
		string numeroMagico, comentario;
		int largura, altura, max;
		char *pixels;

	public:
		Valores();
		Valores(string numeroMagico, string comentario, int largura, int altura, int max, char *pixels);

		string getNumeroMagico();
		void setNumeroMagico(string numeroMagico);

		string getComentario();
		void setComentario(string comentario);

		int getLargura();
		void setLargura(int largura);

		int getAltura();
		void setAltura(int altura);

		int getMax();
		void setMax(int max);

		char * getPixels();
		void setPixels(char *pixels);

		
		
		
};
#endif