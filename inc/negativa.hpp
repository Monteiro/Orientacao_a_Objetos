#ifndef NEGATIVA_HPP
#define NEGATIVA_HPP

#include "filtro.hpp"

using namespace std;

class Negativa : public Filtro {

	public:
		Negativa();
		char * Filtra(char *pixels, int largura, int altura, int *max);
};
#endif