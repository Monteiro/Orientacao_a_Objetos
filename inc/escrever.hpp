#ifndef ESCREVER_HPP
#define ESCREVER_HPP

#include "valores.hpp"

using namespace std;

class Escrever : public Valores {

	private:
		ofstream * saida;

	public:
		Escrever();
		Escrever(ofstream * saida);

		ofstream * getSaida();
		void setSaida(ofstream * saida);

		int Escrita(const char * nome);
};
#endif