#ifndef SHARPEN_HPP
#define SHARPEN_HPP

#include "filtro.hpp"

using namespace std;

class Sharpen : public Filtro {
	public:
		Sharpen();
		char * Filtra(char *pixels, int largura, int altura, int *max);
};
#endif