#include "valores.hpp"

Valores::Valores() {
	setNumeroMagico("P5");
	setComentario("sem cometario");
	setLargura(255);
	setAltura(255);
	setMax(512);
	setPixels(NULL);
}

Valores::Valores(string numeroMagico, string comentario, int largura, int altura, int max, char * pixels) {
	setNumeroMagico(numeroMagico);
	setComentario(comentario);
	setLargura(largura);
	setAltura(altura);
	setMax(max);
	setPixels(pixels);
}

string Valores::getNumeroMagico() {
	return numeroMagico;
}
void Valores::setNumeroMagico(string numeroMagico) {
	this -> numeroMagico = numeroMagico;
}

string Valores::getComentario() {
	return comentario;
}
void Valores::setComentario(string comentario) {
	this -> comentario = comentario;
}

int Valores::getLargura() {
	return largura;
}
void Valores::setLargura(int largura) {
	this -> largura = largura;
}

int Valores::getAltura() {
	return altura;
}
void Valores::setAltura(int altura) {
	this -> altura = altura;
}

int Valores::getMax() {
	return max;
}
void Valores::setMax(int max) {
	this -> max = max;
}

char * Valores::getPixels() {
	return pixels;
}
void Valores::setPixels(char * pixels) {
	this -> pixels = pixels;
}