#include "negativa.hpp"

using namespace std;

Negativa::Negativa() {
}

char * Negativa::Filtra(char *pixels, int largura, int altura, int *max) {
	char *novos_pixels = new char[largura*altura];
	if (novos_pixels == NULL) {
		return NULL;
	}

	for(int i=0; i<largura; i++){
		for(int j=0; j<altura; j++){
			novos_pixels[i+j*largura] = (char)(*max - pixels[i+j*largura]);
		}
	}

	return novos_pixels;
}