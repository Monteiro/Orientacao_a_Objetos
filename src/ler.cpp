#include "ler.hpp"

using namespace std;

Ler::Ler() {
	setEntrada(NULL);
}

Ler::Ler(ifstream *entrada) {
	setEntrada(entrada);
}

ifstream * Ler::getEntrada() {
	return entrada;
}
void Ler::setEntrada(ifstream * entrada) {
	this -> entrada = entrada;
}

int Ler::Leitura(const char *nome) {
	entrada -> open(nome);
	if(!(entrada->is_open())) {
		return -1;
	}

	int altura, largura, max;
	string numeroMagico, comentario;
	stringstream f;

	getline(*entrada, numeroMagico);
	if(numeroMagico != "P5"){
		return -1;
	}

	setNumeroMagico(numeroMagico);
	getline(*entrada, comentario);
	if(comentario[0] == '#') {
		setComentario(comentario);
		* entrada >> largura >> altura;
	}
	else {
		f.str(comentario);
		f >> largura >> altura;
	}
	* entrada >> max;

	setLargura(largura);
	setAltura(altura);
	setMax(max);

	entrada -> get();

	char *pixels = new char[largura*altura];
	if(pixels == NULL) {
		return -1;
	}

	setPixels(pixels);

	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
			entrada->get(pixels[i*largura+j]);
		}
	}

	entrada -> close();

	return 0;
}