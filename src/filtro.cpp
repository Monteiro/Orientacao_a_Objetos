#include "filtro.hpp"

using namespace std;

Filtro::Filtro() {
	setDiv(1);
	setTamanho(3);
}

Filtro::Filtro(int div, int tamanho) {
	setDiv(div);
	setTamanho(tamanho);
}

int Filtro::getDiv() {
	return div;
}
void Filtro::setDiv(int div) {
	this -> div = div;
}

int Filtro::getTamanho() {
	return tamanho;
}
void Filtro::setTamanho(int tamanho) {
	this -> tamanho = tamanho;
}