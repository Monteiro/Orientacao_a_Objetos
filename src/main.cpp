#include <iostream>
#include "ler.hpp"
#include "escrever.hpp"
#include "negativa.hpp"
#include "smooth.hpp"
#include "sharpen.hpp"

using namespace std;

int main(){

	int maximo;
	const char *nomeDaEntrada, *nomeDaSaida;
	string nome_entrada, nome_saida, opcao;
	char *novos_pixels;

	ifstream antes;
	ofstream depois;

	Ler entrada;
	Escrever saida;
	
	Negativa negativa;
	Sharpen sharpen;
	Smooth smooth;

	cout << "Digite o endereco da imagem (Ex: ./doc/lena.pgm): ";
	cin >> nome_entrada;

	nomeDaEntrada = nome_entrada.c_str();
	cout << nome_entrada << endl;

	entrada.setEntrada(&antes);
	saida.setSaida(&depois);
	
	if(entrada.Leitura(nomeDaEntrada)){
		cout << "Impossivel de abrir (arquivo nao encontrado ou nao eh PGM binario) e/ou alocar memoria\n";
		return 0;
	}
	
	cout << endl << "Digite o endereco da imagem gerada (Ex: ./doc/lena-\"nomedofiltro\".pgm): ";
	cin >> nome_saida;
	
	while(1){
		cout << endl << "Escolha o filtro\n";
		cout << "1 para negativo\n" << "2 para smooth\n" << "3 para sharpen\n" << "Opcao: ";
		cin >> opcao;
		if(opcao == "1" || opcao == "2" || opcao == "3")
			break;
		cout << "Opcao invalida, tente novamente\n";
	}
	
	maximo =  entrada.getMax();

	if(opcao == "1"){
		novos_pixels = negativa.Filtra(entrada.getPixels(), entrada.getLargura(), entrada.getAltura(), &maximo);
	}
	else if(opcao == "2"){
		novos_pixels = smooth.Filtra(entrada.getPixels(), entrada.getLargura(), entrada.getAltura(), &maximo);
	}
	else{
		novos_pixels = sharpen.Filtra(entrada.getPixels(), entrada.getLargura(), entrada.getAltura(), &maximo);
	}
	
	if(novos_pixels == NULL){
		cout << "Nao foi possivel alocar memoria" << endl;
		return 0;
	}
	
	nomeDaSaida = nome_saida.c_str();

	saida.setNumeroMagico(entrada.getNumeroMagico());
	saida.setComentario(entrada.getComentario());
	saida.setLargura(entrada.getLargura());
	saida.setAltura(entrada.getAltura());
	saida.setMax(maximo);
	saida.setPixels(novos_pixels);
	
	if(saida.Escrita(nomeDaSaida)){
		cout << "Falha ao gerar novo arquivo" << endl;
		return 0;
	}
	
	cout << "Novo arquivo gerado com sucesso" << endl;

	return 0;
}
