#include "escrever.hpp"

using namespace std;

Escrever::Escrever() {
	setSaida(NULL);
}

Escrever::Escrever(ofstream *saida) {
	setSaida(saida);
}

ofstream * Escrever::getSaida() {
	return saida;
}

void Escrever::setSaida(ofstream * saida) {
	this -> saida = saida;
}

int Escrever::Escrita(const char * nome) {
	saida -> open(nome);

	if(!(saida -> is_open())) {
		return -1;
	}

	char *pixels = getPixels();
	int largura = getLargura();
	int altura = getAltura();

	* saida << getNumeroMagico() << endl;
	if(getComentario() != "vazio") {
		*saida << getComentario() << endl;
	}
		
	*saida << largura << ' ' << altura << endl << getMax() << endl;

	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
				saida->put(pixels[i*largura+j]);
		}
	}

	saida -> close();

	return 0;
}