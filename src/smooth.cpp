#include "smooth.hpp"

using namespace std;

Smooth::Smooth(){
	setDiv(9);
	setTamanho(3);
}

char * Smooth::Filtra(char *pixels, int largura, int altura, int *max){
	int value, i, j; 
	int tamanho = getTamanho(), matriz[] = {1, 1, 1, 1, 1, 1, 1, 1, 1}, div = getDiv();
	char *novos_pixels = new char[largura*altura];
	if(novos_pixels == NULL){
		return NULL;
	}

	for(i=0; i<tamanho; i++){
		for(j=0; j<largura; j++){
			novos_pixels[i+j*largura] = pixels[i+j*largura];
			novos_pixels[(i+j*largura)+largura-1] = pixels[(i+j*largura)+largura-1];
		}
	}
	
	for(i=0; i<tamanho; i++){
		for(j=0; j<altura; j++){
			novos_pixels[i*altura+j] = pixels[i*altura+j];
			novos_pixels[(i+j*largura)+largura-1] = pixels[(i+j*largura)+largura-1];
		}
	}

	for(i=tamanho/2; i<largura-tamanho/2; i++){
		for(j=tamanho/2; j<altura-tamanho/2; j++){
			value = 0;
			for(int x=-(tamanho/2); x<=tamanho/2; x++){
				for(int y=-(tamanho/2); y<=tamanho/2;y++){
					value += matriz[(x+1)+tamanho*(y+1)]*(unsigned char)pixels[(i+x)+(y+j)*largura];
				}
			}
				value /= div;
				
				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;
				
				if(value > *max)
					*max = value;
				
				novos_pixels[i+j*largura] = value;
		}
	}
	
	return novos_pixels;
}
